// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_web_frame/flutter_web_frame.dart';
import 'package:get/get.dart';
import 'package:kyungjoon771234567/pages/MainPage.dart';



void main() {
	runApp(FlutterWebFrame(
		builder: (context) {
			return GetMaterialApp(
				home: MainPage(),
				theme: ThemeData(
					primarySwatch: Colors.orange,
					fontFamily: 'NotoSansKR',
				),
				debugShowCheckedModeBanner: false,
			);
		},
		maximumSize: Size(850.0, 812.0), // Maximum size
		enabled: kIsWeb, // default is enable, when disable content is full size
		backgroundColor: Colors.grey, // Background color/white space
	));
}
