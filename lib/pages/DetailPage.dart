import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kyungjoon771234567/services/SimpleController.dart';


class DetailPage extends StatelessWidget {
	DetailPage({Key? key}) : super(key: key);

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: const Text("단순 상태관리"),
			),
			body: Center(
				child: Row(
					children: [
						ElevatedButton(
							child: Obx(() =>
							(
								Text(
									'현재 숫자: ${SimpleController.to.counter}',
								)
							)),

							onPressed: () {
								SimpleController.to.incrementCounter();
								// Get.find<SimpleController>().increase();
							},
						),
						Obx(() =>
							Text(
								'현재 name: ${SimpleController.to.name}',
							)
						),
						Obx(() =>
							Text(
								'현재 name: ${SimpleController.to.name}',
							)
						),
						Obx(() =>
							Text(
								'현재 name: ${SimpleController.to.name}',
							)
						),
						Obx(() =>
							Text(
								'현재 고경준 천재님이십니다sdlkfsldflsdkflk: ${SimpleController.to.name}',
							)
						),

					],
				)
			),
		);
	}
}