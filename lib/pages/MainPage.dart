// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace, must_be_immutable, prefer_const_literals_to_create_immutables, sort_child_properties_last

import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kyungjoon771234567/services/SharedService.dart';
import 'package:kyungjoon771234567/pages/DetailPage.dart';
import 'package:kyungjoon771234567/services/SimpleController.dart';
import 'package:logger/logger.dart';


var dio = Dio();
var logger = Logger();
var sharedService = SharedService();

class MainPage extends StatelessWidget {
	MainPage({Key? key}) : super(key: key);
	final text1Controller = TextEditingController();

	renderButtons(context, text1Controller) {
		return Row(
			children: [
				ElevatedButton(
					child: Text(
						'snackbar',
					),
					onPressed: () {
						Get.snackbar('Hi', 'i am a modern snackbar',
							snackPosition: SnackPosition.BOTTOM,
							backgroundColor: Colors.pink,
							colorText: Colors.white,
							duration: Duration(milliseconds: 1000),
						);
						//controller.increase();
						// Get.find<SimpleController>().increase();
					},
				),
				ElevatedButton(
					child: Obx(() =>
						Text(
							'현재 숫자: ${SimpleController.to.counter}',
						)
					),
					onPressed: () {
						SimpleController.to.incrementCounter();
						// Get.find<SimpleController>().increase();
					},
				),
				ElevatedButton(
					child: Text(
						'push',
					),
					onPressed: () {
						Navigator.push(
							context,
							MaterialPageRoute(
								builder: (context) => DetailPage(),
								fullscreenDialog: true,
							),
						);
					},
				),
				ElevatedButton(
					child: Text(
						'name',
					),
					onPressed: () {
						showSnackBar(SimpleController.to.name.value);
					},
				),
				ElevatedButton(
					child: Text(
						'post2222',
					),
					onPressed: () {
						SimpleController.to.postInput(text1Controller.text);
					},
				),
			],
		);
	}



	late FocusNode text1FocusNode = FocusNode();

	postInput() async {
		await SimpleController.to.postInput(text1Controller.text);
		text1Controller.text = '';
		text1FocusNode.requestFocus();
	}

	renderTextInput() {
		return Container(
			height: 100,
			margin: EdgeInsets.fromLTRB(10, 100, 10, 10),
			child: Row(
				children: [
					Flexible(
						flex: 24,
						child: Container(
							padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
							child: TextField(
								decoration: InputDecoration(
									border: OutlineInputBorder(),
									labelText: 'plez input text',
								),
								controller: text1Controller,
								textInputAction: TextInputAction.go,
								onChanged: (value) {
									SimpleController.to.setName(value);
								},
								focusNode: text1FocusNode,
								onSubmitted: (value) async {
									postInput();
								},
							),
						)
					),
					Flexible(
						flex: 4,
						child: Container(
							padding: const EdgeInsets.all(10),
							child: ElevatedButton(
								onPressed: () async {
									print('sdlfksdlfklsdkf');
									postInput();
								},
								child: const Text('1111!')
							),
						),
					),
				],
			),
		);
	}

	@override
	Widget build(BuildContext context) {
		Get.put(SimpleController()); // controller 등록



		return Scaffold(
//			appBar: AppBar(
//				title: const Text("단순 상태관리"),
//			),
			body: Center(
				child: Column(
					mainAxisAlignment: MainAxisAlignment.center,
					children: [
						renderButtons(context, text1Controller),
						renderTextInput(),
						Container(
							height: 700,
							child: Obx(() =>
							(
								SimpleController.to.contentLoading.value ? Column(
									children: [
										Container(
											child: CircularProgressIndicator(),
											margin: EdgeInsets.all(25),
										)
									],
								) :
								ScrollConfiguration(
									behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
										PointerDeviceKind.touch,
										PointerDeviceKind.mouse,
									},),
									child: ListView.builder(
										physics: AlwaysScrollableScrollPhysics(),
										scrollDirection: Axis.vertical,
										padding: const EdgeInsets.all(8),
										shrinkWrap: false,
										itemCount: SimpleController.to.items.value.length,
										itemBuilder: (BuildContext context, int index) {
											return InkWell(
												onTap: () {
													sharedService.push(context, DetailPage());
												},
												child: Container(
													height: 50,
													color: Colors.red,
													margin: EdgeInsets.all(5.0),
													padding: EdgeInsets.all(5.0),
													child: Text(SimpleController.to.items.value[index].firstName)
												)
											);
										}
									)

								)
							)),
						),
						Row(
							children: [
								Text('고경준 천재님이십닏sdlfksldkflsdkfl'),
								SizedBox(width:10),
								Text('고경준 천재님이십닏sdlfksldkflsdkfl'),
								SizedBox(width:10),
								Text('고경준 천재님이십닏sdlfksldkflsdkfl'),
							],
						)

					]
				)
			)
			,
		);
	}
}