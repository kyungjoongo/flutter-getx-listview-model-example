// ignore_for_file: prefer_interpolation_to_compose_strings

class People {
	late int userId;
	late String title;
	late String nickName;
	late int age;

	People({
		required this.userId,
		required this.title,
		required this.nickName,
		required this.age,
	});


	@override
	bool operator ==(Object other) =>
		identical(this, other) ||
			(other is People &&
				runtimeType == other.runtimeType &&
				userId == other.userId &&
				title == other.title &&
				nickName == other.nickName &&
				age == other.age
			);


	@override
	int get hashCode =>
		userId.hashCode ^
		title.hashCode ^
		nickName.hashCode ^
		age.hashCode;


	@override
	String toString() {
		// ignore: prefer_adjacent_string_concatenation
		return 'People{' +
			' userId: $userId,' +
			' title: $title,' +
			' nickName: $nickName,' +
			' age: $age,' +
			'}';
	}


	People copyWith({
		int? userId,
		String? title,
		String? nickName,
		int? age,
	}) {
		return People(
			userId: userId ?? this.userId,
			title: title ?? this.title,
			nickName: nickName ?? this.nickName,
			age: age ?? this.age,
		);
	}


	Map<String, dynamic> toMap() {
		return {
			'userId': this.userId,
			'title': this.title,
			'nickName': this.nickName,
			'age': this.age,
		};
	}

	factory People.fromMap(Map<String, dynamic> map) {
		return People(
			userId: map['userId'] as int,
			title: map['title'] as String,
			nickName: map['nickName'] as String,
			age: map['age'] as int,
		);
	}


//</editor-fold>
}