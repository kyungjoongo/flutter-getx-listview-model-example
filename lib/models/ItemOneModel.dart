// ignore_for_file: prefer_interpolation_to_compose_strings

class ItemOneModel {
	late String id;
	late String firstName;
	late String lastName;
	late String email;

//<editor-fold desc="Data Methods">

	ItemOneModel({
		required this.id,
		required this.firstName,
		required this.lastName,
		required this.email,
	});


	@override
	bool operator ==(Object other) =>
		identical(this, other) ||
			(other is ItemOneModel &&
				runtimeType == other.runtimeType &&
				id == other.id &&
				firstName == other.firstName &&
				lastName == other.lastName &&
				email == other.email
			);


	@override
	int get hashCode =>
		id.hashCode ^
		firstName.hashCode ^
		lastName.hashCode ^
		email.hashCode;


	@override
	String toString() {
		// ignore: prefer_adjacent_string_concatenation
		return 'ItemOneModel{' +
			' id: $id,' +
			' firstName: $firstName,' +
			' lastName: $lastName,' +
			' email: $email,' +
			'}';
	}


	ItemOneModel copyWith({
		String? id,
		String? firstName,
		String? lastName,
		String? email,
	}) {
		return ItemOneModel(
			id: id ?? this.id,
			firstName: firstName ?? this.firstName,
			lastName: lastName ?? this.lastName,
			email: email ?? this.email,
		);
	}


	Map<String, dynamic> toMap() {
		return {
			'id': this.id,
			'firstName': this.firstName,
			'lastName': this.lastName,
			'email': this.email,
		};
	}

	factory ItemOneModel.fromMap(Map<String, dynamic> map) {
		return ItemOneModel(
			id: map['id'] as String,
			firstName: map['firstName'] as String,
			lastName: map['lastName'] as String,
			email: map['email'] as String,
		);
	}


//</editor-fold>
}