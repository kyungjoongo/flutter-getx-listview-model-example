import 'package:flutter/material.dart';
import 'package:kyungjoon771234567/models/ItemOneModel.dart';


class ItemOne extends StatelessWidget {
	const ItemOne

	({
	super.key,
	required this.index,
	required this.items,
	required this.context,
	required this.onTap,
	});

	final int index;
	final List<ItemOneModel> items;
	final BuildContext context;
	final void Function() onTap;

	@override
	Widget build(BuildContext context) {
		return (
			Container(
				height: 80,
				margin: const EdgeInsets.all(5),
				color: Colors.orange,
				child: Material(
					//type: MaterialType.transparency,
					//color: Theme.of(context).primaryColor,
					color: Colors.transparent,
					//color: Colors.grey,
					child: InkWell(
						onTap: onTap,
						child: Row(
							children: [
								Container(
									margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
									child: Text('${items[index].firstName}'),
								),
								Container(width: 20,),
								Text(items[index].email),
							],
						)
					)
				)
			)
		);
	}
}