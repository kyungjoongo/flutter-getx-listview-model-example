import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:kyungjoon771234567/models/ItemOneModel.dart';
import 'package:logger/logger.dart';


var dio = Dio();
var logger = Logger();

class SimpleController extends GetxController {

	SimpleController() {
		init();
	}

	init() async {
		this.contentLoading.value = true;
		await getList();
		this.contentLoading.value = false;
	}


	static SimpleController get to => Get.find(); //

	var counter = 0.obs;

	void incrementCounter() {
		counter.value++;
	}

	postInput(pText) async {
		final response = await dio.post(
			'https://asia-northeast3-flutter-test00000001.cloudfunctions.net/api/users',
			data: {
				"firstName": pText,
				"lastName": "go2222222222",
				"email": "vcarter77@naver.com",
				"date": "2012-12-31"
			}
		);

		logger.d(response);
		getList();
	}

	var name = "".obs;

	setName(value) {
		this.name.value = value;
	}


	RxList<ItemOneModel> items = <ItemOneModel>[].obs;
	var loading = true.obs;
	var contentLoading = false.obs;

	setItem(pItems) {
		this.items.value = pItems;

		logger.d(this.items.value);
	}

	setLoading(value) {
		loading.value = value;
	}

	getList() async {
		loading.value = true;
		//final response = await dio.get('https://jsonplaceholder.typicode.com/posts');
		//https://asia-northeast3-flutter-test00000001.cloudfunctions.net/api/users_all
		final response = await dio.get('https://asia-northeast3-flutter-test00000001.cloudfunctions.net/api/users_all');
		List arrList = response.data;
		//logger.i(arrList);

		var _tempItems = <ItemOneModel>[];
		for (var element in arrList) {
			_tempItems.add(ItemOneModel.fromMap(element));
		}

		this.items.value = _tempItems;
		loading.value = false;
	}

}