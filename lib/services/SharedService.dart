// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';

showSnackBar(text) {
	Get.snackbar('', text,
		snackPosition: SnackPosition.BOTTOM,
		backgroundColor: Colors.orangeAccent,
		colorText: Colors.white,
		duration: Duration(milliseconds: 1500),
		//padding: EdgeInsets.all(5.0),
		margin: EdgeInsets.all(10.0),
	);
}

class SharedService {

	push(context, page) {
		Navigator.push(
			context,
			MaterialPageRoute(
				builder: (context) => page,
				fullscreenDialog: true,
			),
		);
	}
}