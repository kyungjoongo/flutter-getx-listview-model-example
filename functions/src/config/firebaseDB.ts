import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

const {getStorage} = require('firebase-admin/storage');
const serviceAccount = require('../config/kyungjoon-start01-firebase-adminsdk-2my9j-02c0921eed.json');
import {storage} from 'firebase-admin'

admin.initializeApp(
    {
        credential: admin.credential.cert(serviceAccount),
        storageBucket: "kyungjoon-start01.appspot.com"
    },
    functions.config().firebase
);

export const firebaseDB = admin.firestore();



