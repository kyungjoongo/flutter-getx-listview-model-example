import * as functions from "firebase-functions";
import * as express from 'express';
import * as bodyParser from "body-parser";
import userRouter from "./routers/UserRouter";
import clientRouter from "./routers/ClientRouter";
import * as cors from 'cors'
const expressApp = express();
expressApp.use(bodyParser.json());
expressApp.use(cors({origin: true}));


expressApp.use(bodyParser.urlencoded({extended: false}));
//todo: #####################
//todo: routers
//todo: #####################
expressApp.use("/", userRouter)
expressApp.use("/", clientRouter)


export const api = functions.region('asia-northeast3').https.onRequest(expressApp);

