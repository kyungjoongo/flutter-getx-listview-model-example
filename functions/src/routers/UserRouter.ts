import * as express from "express";
import {firebaseDB} from "../config/firebaseDB";
const userRouter = express.Router();
const userCollection = 'users';

//add user
userRouter.post('/users', async (req, res) => {
    try {
        const user: User = {
            firstName: req.body['firstName'],
            lastName: req.body['lastName'],
            email: req.body['email'],
            date: new Date(),
        }
        const newDoc = await firebaseDB.collection(userCollection).add(user);

        res.status(201).send({
            status : 201,
            data : `Created a new user: ${newDoc.id}`
        });
    } catch (error) {
        res.status(400).send(`User should cointain firstName, lastName, email, areaNumber, department, id and contactNumber!!!`)
    }
});


//todo: get a userList for paging
userRouter.get('/users', async (req, res) => {
    try {

        let pageSize = 10;
        let page = req.query.page
        let snapshot = null
        if (Number(page) === 1) {
            snapshot = firebaseDB.collection(userCollection).orderBy('date', 'desc').limit(pageSize)
        } else {
            let _snapshot = firebaseDB.collection(userCollection).orderBy('date', 'desc').limit(((Number(page) - 1) * pageSize))
            let __documentSnapShots = await _snapshot.get()
            const last = __documentSnapShots.docs[__documentSnapShots.docs.length - 1];
            snapshot = firebaseDB.collection(userCollection).orderBy('date', 'desc').startAfter(last.data().date).limit(pageSize);
        }
        const currentSnapShot = await snapshot.get();
        const users: any[] = [];
        currentSnapShot.forEach(doc => {
            users.push({
                id: doc.id,
                ...doc.data()
            });
        })

        res.status(200).json(users);
    } catch (error) {
        res.status(500).send(error);
    }
});

userRouter.get('/users_all', async (req, res) => {
    try {
        let page = req.query.page
        //let _ref =firebaseDB.collection(userCollection)

        let snapshot = null
        snapshot = firebaseDB.collection(userCollection).orderBy('date', 'desc')
        const currentSnapShot = await snapshot.get();
        const users: any[] = [];
        currentSnapShot.forEach(doc => {
            users.push({
                id: doc.id,
                ...doc.data()
            });
        })

        res.status(200).json(users);
    } catch (error) {
        res.status(500).send(error);
    }
});


//get a single contact
userRouter.get('/users/:userId', (req, res) => {

    const userId = req.params.userId;
    firebaseDB.collection(userCollection).doc(userId).get()
        .then(user => {
            if (!user.exists) throw new Error('User not found');
            res.status(200).json({id: user.id, data: user.data()})
        })
        .catch(error => res.status(500).send(error));

});

// Delete a user
userRouter.delete('/users/:userId', (req, res) => {
    firebaseDB.collection(userCollection).doc(req.params.userId).delete()
        .then(() => res.status(204).send("Document successfully deleted!"))
        .catch(function (error) {
            res.status(500).send(error);
        });
})

// Update user
userRouter.put('/users/:userId', async (req, res) => {
    await firebaseDB.collection(userCollection).doc(req.params.userId).set(req.body, {merge: true})
        .then(() => res.json({id: req.params.userId}))
        .catch((error) => res.status(500).send(error))

});

userRouter.get('/', async (req, res) => {
    res.status(400).send(`firebase function started!!!!!!`)
});

export default userRouter;
