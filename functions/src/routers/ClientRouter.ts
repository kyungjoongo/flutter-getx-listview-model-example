import * as express from "express";
import {firebaseDB} from "../config/firebaseDB";

const clientRouter = express.Router();

//add user
clientRouter.post('/clients', async (req, res) => {
    try {
        const client: Client = {
            name: req.body.name,
            nickname: req.body.nickname,
            education: req.body.education,
            age: req.body.age,
            location: req.body.location,
            price: req.body.price,
        }
        const newDoc = await firebaseDB.collection('clients').add(client);
        res.status(201).send(`Created a new user: ${newDoc.id}`);
    } catch (error) {
        res.status(400).send(`User should cointain firstName, lastName, email, areaNumber, department, id and contactNumber!!!`)
    }
});


//get a userList
clientRouter.get('/clients', async (req, res) => {
    try {
        const userQuerySnapshot = await firebaseDB.collection('clients').get();
        const users: any[] = [];
        userQuerySnapshot.forEach(
            (doc) => {
                users.push({
                    id: doc.id,
                    data: doc.data()
                });
            }
        );
        res.status(200).json(users);
    } catch (error) {
        res.status(500).send(error);
    }
});

//get a single contact
clientRouter.get('/clients/:userId', (req, res) => {

    const userId = req.params.userId;
    firebaseDB.collection('clients').doc(userId).get()
        .then(user => {
            if (!user.exists) throw new Error('User not found');
            res.status(200).json({id: user.id, data: user.data()})
        })
        .catch(error => res.status(500).send(error));

});

// Delete a user
clientRouter.delete('/clients/:id', (req, res) => {
    firebaseDB.collection('clients').doc(req.params.id).delete()
        .then(() => res.status(204).send("Document successfully deleted!"))
        .catch(function (error) {
            res.status(500).send(error);
        });
})

// Update user
clientRouter.put('/clients/:id', async (req, res) => {
    await firebaseDB.collection('clients').doc(req.params.id).set(req.body, {merge: true})
        .then(() => res.json({id: req.params.id}))
        .catch((error) => res.status(500).send(error))

});

clientRouter.get('/', async (req, res) => {
    res.status(400).send(`firebase function started!!!!!!`)
});

export default clientRouter;
